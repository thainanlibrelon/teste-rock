var botaoAddMember = document.querySelector(".btn-create-member");

var membroCount = 0;

botaoAddMember.addEventListener("click", function(event){
	event.preventDefault();

	var form = document.querySelector('#form-addmember');
	var nome = form.nome.value;
	var email = form.email.value;

	var membroGrid = document.createElement("div")
	membroGrid.className = "col-lg-3";

	var membroBox = document.createElement("div");
	membroBox.className = "team-member text-center";
	membroBox.setAttribute("id","member" + membroCount);

	var membroFotoDiv = document.createElement("div");
	membroFotoDiv.className = "member-photo";

	var membroNome = document.createElement("div");
	membroNome.className = "member-name";

	var membroEmail = document.createElement("div");
	membroEmail.className = "member-e-mail";

	var membroFoto = document.createElement("img");
	membroFoto.className = "user-photo";
	membroFoto.setAttribute("src", "images/Ball-01.png")

	var membroEdit = document.createElement("button");
	membroEdit.className = "btn-edit";
	membroEdit.setAttribute("data-id", "member" + membroCount);
	membroEdit.innerHTML = "<i class='fa fa-pencil' aria-hidden='true'></i>";

	membroNome.textContent = nome;
	membroEmail.textContent = email;

	membroFotoDiv.appendChild(membroFoto);
	membroBox.appendChild(membroFotoDiv);	
	membroBox.appendChild(membroNome);
	membroBox.appendChild(membroEmail);
	membroBox.appendChild(membroEdit);
	membroGrid.appendChild(membroBox);

	var membroRow = document.querySelector("#members-list");
	
	membroRow.appendChild(membroGrid);

	membroCount++;

	/*editFunction();*/
});


/*
var myFunction =  function(event){

	var id = this.getAttribute("data-id");
	var name = $(id).find('member-name').text();
	var email = $(id).find('member-email').text();

	editarForm(id, name, email);


};

function editarForm(id, name, email) {

	append(id);
	append(name);
	append(mail);

	//esconder botao criar
	var btnCriar = document.querySelector(".btn-create-member");
	btnCriar.className = "hidden";
	//mostrar botao de editar
	var btnEditar = document.getElementsByClassName("btn-edit-member");
	btnEditar.removeAttribute("class", "hidden");
};

var botaoEditMember = document.querySelector(".btn-edit-member");
botaoEditMember.addEventListener("click", function(event){

}

function aplicarEdicao() {
	var form = document.querySelector('#form-addmember');
	var id = form.id.value;
	var nome = form.nome.value;
	var email = form.email.value;

	var teste = document.querySelector("#" + id);

	teste.find('member-name').text(nome);
	teste.find('member-email').text(email);
}

var editFunction = function(){

	var botaoEditMember = document.getElementsByClassName("btn-edit");

	//botaoEditMember.removeEventListener("click", myFunction);
	for (var i = 0; i < botaoEditMember.length; i++) {
    	botaoEditMember[i].addEventListener('click',myFunction);
	}


};

*/